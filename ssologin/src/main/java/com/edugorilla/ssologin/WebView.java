package com.edugorilla.ssologin;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.webkit.JavascriptInterface;
import android.webkit.WebViewClient;

public class WebView extends AppCompatActivity {
    private ProgressDialog progress_dialog;
    private android.webkit.WebView web_view;
    private Boolean is_full_screen_status = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        progress_dialog = ProgressDialog.show(this, "", "Please wait...", true);
        progress_dialog.setCancelable(false);

        String url = getIntent().getStringExtra("url");

        web_view = findViewById(R.id.web_view);
        web_view.getSettings().setJavaScriptEnabled(true);
        web_view.getSettings().setLoadWithOverviewMode(true);
        web_view.getSettings().setUseWideViewPort(true);
        web_view.getSettings().setDomStorageEnabled(true);
        web_view.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(android.webkit.WebView view, final String url) {
                if (progress_dialog.isShowing()) {
                    progress_dialog.dismiss();
                }
            }
        });
        web_view.loadUrl(url);
        web_view.addJavascriptInterface(new WebView.WebViewInterface(this), "Android");
    }

    public class WebViewInterface {

        private Activity activity;

        public WebViewInterface(Activity activity) {
            this.activity = activity;
        }

        @JavascriptInterface
        public void goToBack() {
            finish();
        }

        @JavascriptInterface
        public String getFullScreenStatus() {
            // This getFullScreenStatus method is used to detect whether the user currently active or not.
            // If the user minimizes the app, `is_full_screen_status` is set to `false`, meaning the user is not currently active.
            // When the user returns to the app, a pop-up dialog will appear with the message: "Please enable full screen."
            // If the user leaves again, the test will be automatically submitted.
            return String.valueOf(is_full_screen_status);
        }

        @JavascriptInterface
        public void toggleFullScreenStatus() {
            is_full_screen_status = !is_full_screen_status;
        }

    }

    @Override
    public void onBackPressed() {
        if (web_view.canGoBack()) {
            web_view.goBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        is_full_screen_status = false;
    }
}